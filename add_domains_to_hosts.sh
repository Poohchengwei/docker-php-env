#!/bin/bash

# Array of domains to add to /etc/hosts
domains=("fusion-local.spgamesmanager.net" "local.spmobileapi.net" "phpmyadmin" "nb-api.localhost.net" "nb-admin.localhost.net"  "easypay-api.localhost.net"  "easypay-admin.localhost.net" "laravel.localhost.net")

# Loop through the array and check/add each domain to /etc/hosts
for domain in "${domains[@]}"; do
  if ! grep -q "127.0.0.1       $domain" /etc/hosts; then
    echo "127.0.0.1       $domain" | sudo tee -a /etc/hosts
    echo "Added $domain to /etc/hosts"
  fi
done